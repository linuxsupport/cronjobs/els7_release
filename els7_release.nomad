job "${PREFIX}_els7_release" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_els7_release" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/els7_release/els7_release:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_els7_release"
        }
      }
      mounts = [
        {
          type = "bind"
          target = "/mnt/data1"
          source = "/mnt/data1"
          readonly = true
          bind_options = {
            propagation = "rshared"
          }
        }
      ]
      volumes = [
        "/etc/nomad/linuxci_pw:/etc/linuxci.credential",
        "$ADVISORIES:/advisories"
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      ELS_SOURCE_PACKAGES = "$ELS_SOURCE_PACKAGES"
      DIST_SOURCE_PACKAGES = "$DIST_SOURCE_PACKAGES"
      RESULT_DIR = "$RESULT_DIR"
      KOJI_PROFILE = "$KOJI_PROFILE"
      ELS_SKIPLIST = "$ELS_SKIPLIST"
      LINUX_WEB_GIT_BRANCH = "$LINUX_WEB_GIT_BRANCH"
      TAG = "${PREFIX}_els7_release"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 2048 # MB
    }

  }
}
