SCHEDULE="30 5 * * *"
ADMIN_EMAIL="lxsoft-admins@cern.ch"
ELS_SOURCE_PACKAGES="/mnt/data1/dist/cdn.redhat.com/content/els/rhel/server/7/7Server/x86_64/source/SRPMS/Packages/"
DIST_SOURCE_PACKAGES="/mnt/data1/dist/cdn.redhat.com/content/dist/rhel/server/7/7Server/x86_64/source/SRPMS/Packages/"
RESULT_DIR="/mnt/data1/dist/internal/repos/els7-stable/source/SRPMS"
# ELS_SKIPLIST is comma separated
ELS_SKIPLIST="rhc-worker-script,tigervnc-1.8.0-34.el7_9,host-metering,freeradius-3.0.20-1.el7_9.1.1,krb5-1.15.1-55.el7_9.3.1"
KOJI_PROFILE="koji"
LINUX_WEB_GIT_BRANCH="master"
ADVISORIES="/mnt/data2/advisories/RHEL7ELS/x86_64"
