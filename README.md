# els7_release

`els7_release` is a script that runs via nomad to rebuild ELS (Extended Lifecycle Support) packages from RedHat ELS src rpms.


`els7_release` will:
* compare the package list from the main distribution and the ELS content
* confirm if there are any packages already present in the els7-stable tag, and skip building these if they already exist
* build packages via koji
* tag built packages to the els7-stable tag
* send an informational email to administrators detailing the build success/failures in the event of new ELS packages
* Update the linux.cern.ch webpage (Updates -> ELS7)
